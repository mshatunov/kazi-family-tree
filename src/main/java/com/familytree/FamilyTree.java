package com.familytree;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FamilyTree {

    public static final int GENERATIONS_TO_CHECK = 100;
    private static final String SPLIT_BY_SPACES = "\\s+";
    /**
     * Declare necessary variables to describe your Tree
     * Each Node in the Tree represents a person
     * You can declare other classes if necessary
     */

    private List<Person> persons = new ArrayList<>();

    public FamilyTree() {
    }

    /**
     * @param familyFile
     * @throws Exception
     * @input directory or filename of input file. This file contains the information necessary to build the child
     * parent relation. Throws exception if file is not found
     */

    public void buildFamilyTree(String familyFile) throws Exception {
        File file = new File(familyFile).getAbsoluteFile();
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                System.out.println("--------------------");
                System.out.println("Adding new family:");

                String family = scanner.nextLine();
                String[] familyMembers = family.trim().split(SPLIT_BY_SPACES);

                Person father = findPersonOrCreateNew(familyMembers[0]);
                System.out.println("Father - " + father);

                Person mother = findPersonOrCreateNew(familyMembers[1]);
                System.out.println("Mother - " + mother);

                Stream.of(familyMembers)
                        .skip(2)
                        .forEach(c -> processChild(father, mother, c));
            }
        }
    }

    private void processChild(Person father, Person mother, String name) {
        Person child = findPersonOrCreateNew(name);
        System.out.println("Child - " + child);
        mother.addChild(child);
        father.addChild(child);
        child.addParent(mother);
        child.addParent(father);
    }

    private Person findPersonOrCreateNew(String personName) {
        return persons.stream()
                .filter(p -> p.getName().equals(personName))
                .findFirst()
                .orElseGet(() -> createNewPerson(personName));
    }

    private Person createNewPerson(String person) {
        Person newPerson = new Person(person);
        persons.add(newPerson);
        return newPerson;
    }

    /**
     * @param queryFile
     * @param outputFile
     * @throws Exception
     * @input directory or filename of Query and Output.
     * queryFile contains the queries about the tree.
     * The output of this query should be written in file outputfile.
     */

    public void evaluate(String queryFile, String outputFile) throws Exception {
        /*
         * Traverse the tree to answer the queries
         * For information on queries take a look at the handout
         */
        File qFile = new File(queryFile).getAbsoluteFile();
        File oFile = new File(outputFile).getAbsoluteFile();

        try (Scanner scanner = new Scanner(qFile);
             BufferedWriter writer = new BufferedWriter(new FileWriter(oFile, true))) {

            while (scanner.hasNext()) {
                System.out.println("----------------------------------------");
                String[] query = scanner.nextLine().trim().split(SPLIT_BY_SPACES);

                Person first = findPersonOrCreateNew(query[0]);
                Person second = findPersonOrCreateNew(query[1]);

                System.out.println("Is " + first + " and " + second + " are related?");
                if (!isDescendant(first, second, writer) && !isDescendant(second, first, writer)) {
                    foundRelativeConnections(first, second, writer);
                }
                writer.write("\n");
                System.out.println("\n");
            }
        }
    }

    private boolean isDescendant(Person first, Person second, BufferedWriter writer) throws Exception {
        Set<Person> children = new HashSet<>(second.getChildren());
        for (int i = 0; i < GENERATIONS_TO_CHECK; i++) {
            Set<Person> people = children.stream()
                    .flatMap(c -> c.getChildren().stream())
                    .collect(Collectors.toSet());
            children.addAll(people);
            if (checkDescendantCondition(first, second, writer, children))
                return true;
        }
        return false;
    }

    private boolean checkDescendantCondition(Person first, Person second, BufferedWriter writer, Set<Person> children) throws IOException {
        if (children.contains(first)) {
            System.out.println(first + " is a descendant of " + second);
            writer.write(first + " is a descendant of " + second);
            return true;
        }
        return false;
    }

    private void foundRelativeConnections(Person first, Person second, BufferedWriter writer) throws Exception {
        Set<Person> potentialRelativesFirst = new HashSet<>(first.getParents());
        Set<Person> potentialRelativesSecond = new HashSet<>(second.getParents());

        Set<Person> personList = potentialRelativesFirst.stream()
                .filter(potentialRelativesSecond::contains)
                .collect(Collectors.toSet());
        if (checkRelativeCondition(writer, personList))
            return;

        for (int i = 0; i < GENERATIONS_TO_CHECK; i++) {
            Set<Person> firstParents = potentialRelativesFirst.stream()
                    .flatMap(c -> c.getParents().stream())
                    .collect(Collectors.toSet());
            potentialRelativesFirst.addAll(firstParents);

            Set<Person> secondParents = potentialRelativesSecond.stream()
                    .flatMap(c -> c.getParents().stream())
                    .collect(Collectors.toSet());
            potentialRelativesSecond.addAll(secondParents);

            personList = potentialRelativesFirst.stream()
                    .filter(potentialRelativesSecond::contains)
                    .collect(Collectors.toSet());

            if (checkRelativeCondition(writer, personList))
                return;
        }

        System.out.println("unrelated");
        writer.write("unrelated");
    }

    private boolean checkRelativeCondition(BufferedWriter writer, Set<Person> personList) {
        if (!personList.isEmpty()) {
            personList.stream().sorted().forEach(x -> printSeveral(x, writer));
            return true;
        }
        return false;
    }

    private void printSeveral(Person x, BufferedWriter writer) {
        try {
            writer.write(x + " ");
            System.out.print(x + " ");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class Person implements Comparable {
        private List<Person> parents = new ArrayList<>();
        private String name;
        private List<Person> children = new ArrayList<>();

        public Person(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public List<Person> getChildren() {
            return children;
        }

        public void addChild(Person child) {
            children.add(child);
        }

        public List<Person> getParents() {
            return parents;
        }

        public void addParent(Person parent) {
            parents.add(parent);
        }

        @Override
        public String toString() {
            return name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Person person = (Person) o;
            return Objects.equals(getName(), person.getName());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getName());
        }

        @Override
        public int compareTo(Object p) {
            Person otherPerson = (Person) p;
            return this.getName().compareTo(otherPerson.getName());
        }
    }
}

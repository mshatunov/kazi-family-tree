package com.familytree;

import org.junit.Test;

import java.io.File;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FamilyTreeTest {

    @Test
    //@ScoringWeight(0.5)
    public void testOne() throws Exception {
        checkFile(1);
    }

    @Test
    //@ScoringWeight(0.5)
    public void testTwo() throws Exception {
        checkFile(2);
    }

    @Test
    //@ScoringWeight(0.5)
    public void testThree() throws Exception {
        checkFile(3);
    }

    @Test
    //@ScoringWeight(0.5)k
    public void testFour() throws Exception {
        checkFile(4);
    }

    @Test
    //@ScoringWeight(0.5)
    public void testFive() throws Exception {
        checkFile(5);
    }

    private void checkFile(int fileNo) {
        File f = new File("files/out/out" + fileNo + ".txt");
        runCase(fileNo);
        assertTrue("Output file " + fileNo + " is not created yet", f.exists());
        assertEquals(true, matchFiles("files/correctout/out" + fileNo + ".txt", "files/out/out" + fileNo + ".txt"));
    }

    public void runCase(int fileNo) {
        try {
            FamilyTree familyTree = new FamilyTree();
            //File f = new File("files/in/input" + fileNo + ".txt");
            //assertTrue(f.exists());
            familyTree.buildFamilyTree("files/in/input" + fileNo + ".txt");
            familyTree.evaluate("files/in/query" + fileNo + ".txt", "files/out/out" + fileNo + ".txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean matchFiles(String real, String out) {
        try {
            Scanner inf1 = new Scanner(new File(real));
            Scanner inf2 = new Scanner(new File(out));

            while (inf1.hasNextLine()) {
                String actual = inf1.nextLine();
                actual = actual.trim();
                if (actual.length() == 0) break;
                if (!inf2.hasNextLine()) return false;

                String got = inf2.nextLine();
                got = got.trim();
                assertEquals(actual, got);
                /*
                if(actual.compareTo(got)!=0){
                    return false;
                }
                */
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }
}